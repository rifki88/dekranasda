<?php
define("URL","http://pel.dephub.go.id/DEPHUB/generic/gerfal");
$has = ($success * 2.25);

$date = date('Ymd');
//$has = 90;
$token = md5("".$date."|".$id_pel."|".$has."");
//$token = md5("20190912|GysyROzgp4UP2hQIx0PNawS__Y|60.00");
//echo $token;
$fields = array(
  'token'=>$token,
  'key'=>$id_pel,
  'subjectScore'=>$has
);
$d = json_encode($fields);
$ch = curl_init();
curl_setopt($ch, CURLOPT_URL, URL);
curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
curl_setopt($ch, CURLOPT_POSTFIELDS, $d);
curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
curl_setopt($ch, CURLOPT_HTTPHEADER, array(
    'Content-Type: application/json',
    'Content-Length: ' . strlen($d))
);
$result = curl_exec($ch);
print_r($result);
curl_close($ch);
?>
<div class="content content-fixed">
  <div class="container pd-x-0 pd-lg-x-10 pd-xl-x-0">

    <div class="card row row-xs pd-20">
        <div class="col-md-6 offset-md-3 tx-center">
          <img src="<?= base_url(); ?>assets/img/kemenhub.png" class="img-fluid" alt="Logo Kemenhub" width="50">
          <br>
          <h6 style="font-size: 18pt;">DIREKTORAT JENDRAL KELAIKUDARAAN DAN PENGOPERASIAN PESAWAT UDARA</h6>
          <br>
          <div class="row col-md-12">
            <div class="col-md-6">
              <h5 class="mg-b-2 tx-spacing--1"><?= $username; ?></h5>
              <p class="tx-color-03 mg-b-25"><?= $status; ?></p>
              <div class="col-sm-12 col-md-12 col-lg-12">
                <ul class="list-unstyled profile-info-list" style="text-align: center;">
                  <li><span>Nomor Ujian</span><span class="tx-color-03"><br> <?= $email; ?></span></li><br>
                  <li><span>Jenis Ujian</span><span class="tx-color-03"><br> WTI AIRLAW</span></li><br>
                  <li><span>Tanggal Ujian</span><span class="tx-color-03"><br> 14 Feb 2020</span></li><br>
                  <li><span>Lokasi Ujian </span><span class="tx-color-03"><br> Indonesia </span></li>
                </ul>
              </div>
            </div>
            <div class="col-md-6">
              <br>
              <br>
              <br>
              <br>
              <span><h1>NILAI ANDA</h1></span>
              <span><h1><?php echo ($success * 2.25); ?></h1></span>
            </div>
          </div>
          <div class="row col-md-6 offset-md-5 tx-center">
              <a class="btn btn-success" href="<?= base_url('user/logout'); ?>">OK</a>
            </div>
        </div>
    </div><!-- row -->

  </div><!-- container -->
</div><!-- content -->
